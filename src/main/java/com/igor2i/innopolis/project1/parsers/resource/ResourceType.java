package com.igor2i.innopolis.project1.parsers.resource;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by igor2i on 12.02.17.
 */
public enum ResourceType {
    FILE, WEBSITE, NOT_SUPPORT;

    public static boolean isWebType(String link){

        Pattern p = Pattern.compile("^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]");
        Matcher m = p.matcher(link);
        return m.matches();
    }
}
