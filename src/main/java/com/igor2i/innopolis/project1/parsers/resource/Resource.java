package com.igor2i.innopolis.project1.parsers.resource;

import java.io.InputStreamReader;

/**
 * Контейнер ресурса
 * содержит InputStreamReader на данные первичного ресурса
 * @author igor2i on 12.02.17.
 */
public class Resource {

    private ResourceType resourceType;
    private String resourceName;
    private InputStreamReader inputStreamReader;

    public Resource() {
    }

    public Resource(ResourceType resourceType, String resourceName, InputStreamReader inputStreamReader) {
        this.resourceType = resourceType;
        this.resourceName = resourceName;
        this.inputStreamReader = inputStreamReader;
    }

    public ResourceType getResourceType() {
        return resourceType;
    }

    public void setResourceType(ResourceType resourceType) {
        this.resourceType = resourceType;
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public InputStreamReader getInputStreamReader() {
        return inputStreamReader;
    }

    public void setInputStreamReader(InputStreamReader inputStreamReader) {
        this.inputStreamReader = inputStreamReader;
    }
}
