package com.igor2i.innopolis.project1.parsers.web;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URL;

/**
 * Created by igor2i on 14.02.17.
 */
public class ParserLibraryOfBabel extends ParserWebPage {

    {
        super.pointOfEntry = "pre[id*=textblock]";
    }


    public ParserLibraryOfBabel() {
    }

    public ParserLibraryOfBabel(URL url) {

        super(url);
    }

    public ParserLibraryOfBabel(String url) throws IOException {
        super(url);
    }

    @Override
    public String getBodyPaper(){

        Elements metaElements = super.getDocument().select(pointOfEntry);
        return Jsoup.clean(metaElements.html(), Whitelist.simpleText().removeTags("b","i"));

    }

}
