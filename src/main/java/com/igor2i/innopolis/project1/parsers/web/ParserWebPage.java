package com.igor2i.innopolis.project1.parsers.web;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URL;


/**
 * Created by igor2i on 06.02.17.
 */
public class ParserWebPage {

    private static final Logger LOGGER = LogManager.getLogger(ParserWebPage.class);

    private URL url;
    protected String pointOfEntry = "body";
    private Document document;

    public ParserWebPage() {
    }

    public ParserWebPage(URL url) {
        this.url = url;
    }

    public ParserWebPage(String url) throws IOException {
        this.url = new URL(url);
    }

    /**
     *
     * @param url
     * @param pointOfEntry - точка
     * @throws IOException
     */
    public ParserWebPage(String url, String pointOfEntry) throws IOException {
        this.url = new URL(url);
        this.pointOfEntry = pointOfEntry;

    }

    public String getBodyPaper() throws IOException {

        return knownSite(url);

    }

    private String knownSite(URL url) throws IOException {

        this.document = Jsoup.parse(url,10000);
        String host = url.getHost();
        if(host.contains("wikipedia.org")){

            ParserWebPage parserwiki = new ParserWiki(url);
            parserwiki.setDocument(document);
            return parserwiki.getBodyPaper();

        }else if(host.contains("libraryofbabel.info")){

            ParserWebPage parserLibraryOfBabel = new ParserLibraryOfBabel(url);
            parserLibraryOfBabel.setDocument(document);
            return parserLibraryOfBabel.getBodyPaper();

        }else{

            Elements metaElements = getDocument().select(pointOfEntry);
            return Jsoup.clean(metaElements.html(), Whitelist.simpleText().removeTags("b","i"));

        }

    }


    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }
}
