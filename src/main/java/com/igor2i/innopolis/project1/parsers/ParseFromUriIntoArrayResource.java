package com.igor2i.innopolis.project1.parsers;

import com.igor2i.innopolis.project1.parsers.resource.Resource;
import com.igor2i.innopolis.project1.parsers.resource.ResourceType;
import com.igor2i.innopolis.project1.parsers.web.ParserWebPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.ArrayList;

import static com.igor2i.innopolis.project1.Main.isWork;
import static com.igor2i.innopolis.project1.Main.setShutdown;

/**
 * Данный клас читает файл, ищет в нём ссылки на файлы или другие типы ресурсов и провиряет их на доступность,
 * при достпности ресурс передаётся в ArrayList.
 *
 * @ForFiles Файлы должны находиться в одной папке с inFile, или лежать дальше по пути.
 * @author  igor2i on 08.02.17.
 */
public class ParseFromUriIntoArrayResource {

    private static final Logger LOGGER = LogManager.getLogger(ParseFromUriIntoArrayResource.class);

    private File inFile;
    private ArrayList<Resource> resourceArrayList = new ArrayList<>(20);


    public ParseFromUriIntoArrayResource(File file) {
        this.inFile = file;
    }

    /**
     * Использует ссылку на ресурс переданный при создании объекта
     * при построчном переборе проверяет ссылки на файлы, и отдаёт в массив проверенные доступные ресурсы.
     * @return ArrayList<Resource> - массив с коректными ссылками на файлы
     */
    public ArrayList<Resource> startParsURL(){

        try {
            LineNumberReader reader = new LineNumberReader(new InputStreamReader(new FileInputStream(inFile)));
            String line = reader.readLine();
            while (line != null && isWork()) {
                LOGGER.debug(line);
                try {
                    resourceArrayList.add(detectedResource(line));
                }catch (IOException | IllegalArgumentException e){
                    LOGGER.warn("Resource not support " + line);
                }

                line = reader.readLine();
            }
            reader.close();


        } catch (FileNotFoundException e) {
            LOGGER.error("Your file "+ inFile.getName() +" not found " + e);
            setShutdown();
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
            setShutdown();
        }

        return resourceArrayList;

    }

    /**
     * Метод принимет ссылку на ресурс и в зависимости от типа ресурса
     * возврощает ресурс с указанием его типа и
     * @param line
     * @return Resource
     * @throws IOException
     */
    private Resource detectedResource(String line) throws IOException {

        Resource resource = new Resource();

        if (ResourceType.isWebType(line)){

            resource.setResourceType(ResourceType.WEBSITE);
            resource.setResourceName(line);

            ParserWebPage parserWebPage = new ParserWebPage(line);
            resource.setInputStreamReader(new InputStreamReader(new ByteArrayInputStream(parserWebPage.getBodyPaper().getBytes())));

            LOGGER.debug("Founded path for words in the website:" + resource.getResourceName());

        }else {

            File readFile = new File(inFile.getParent() + "/" + line);
            if (readFile.isFile()){

                resource.setResourceName(line);
                resource.setResourceType(ResourceType.FILE);
                resource.setInputStreamReader(new InputStreamReader(new FileInputStream(readFile)));

                LOGGER.debug("Founded path for words in the file:" + readFile.getName());
            }else {
                throw new IllegalArgumentException(line);
            }
        }

        return resource;
    }

}
