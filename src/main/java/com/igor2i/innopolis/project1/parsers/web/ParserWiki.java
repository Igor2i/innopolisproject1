package com.igor2i.innopolis.project1.parsers.web;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URL;

/**
 * Данный класс преднозначен для парсинга контента со страниц Wikipedia
 */
public class ParserWiki extends ParserWebPage {

    {
        super.pointOfEntry = "div[id*=mw-content-text]";
    }

    public ParserWiki() {
    }

    public ParserWiki(String url) throws IOException {
        super(url);
    }

    public ParserWiki(URL url) throws IOException {
        super(url);
    }

    /**
     *
     * @return only text paper
     */
    @Override
    public String getBodyPaper(){

        Elements metaElements = super.getDocument().select(pointOfEntry).select("p");
        return Jsoup.clean(metaElements.html(), Whitelist.simpleText().removeTags("b","i"));

    }


}
