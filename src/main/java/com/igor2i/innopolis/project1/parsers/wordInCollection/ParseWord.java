package com.igor2i.innopolis.project1.parsers.wordInCollection;

import com.igor2i.innopolis.project1.Main;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.HashSet;

import static com.igor2i.innopolis.project1.Main.isWork;

/**
 * @author igor2i on 08.02.17.
 */
public class ParseWord {

    private static final Logger LOGGER = LogManager.getLogger(ParseWord.class);

    private InputStreamReader inputSR;
    private HashSet<String> hashDictionary;
    private Object lock;

    /**
     * Передаётся поток, читаемый построчно, и задаётся коллекция для вывода обработаных слов
     * @param inputSR
     * @param hashDictionary
     * @param lock
     */
    public ParseWord(Object lock, InputStreamReader inputSR, HashSet<String> hashDictionary) {
        this.lock = lock;
        this.inputSR = inputSR;
        this.hashDictionary = hashDictionary;
    }

    public void newParse(){
        try {

            LineNumberReader reader = new LineNumberReader(inputSR);
            String line = reader.readLine();
            while (line != null && isWork()) {

                LOGGER.debug(line);

                pars(line);

                line = reader.readLine();

            }
            reader.close();

        } catch (IOException e) {
            LOGGER.warn("Ooops... InputStream is down " + e.getMessage());
        }
    }

    /**
     * Передаётся строка такста, которая обрабаты вается с помощью регулярного выражения
     * и передаётся коллекциии заданой при объявлении класса.
     * @param line - строка String
     */
    private void pars(String line) {
        // разделяет на слова
        String[] parts = (line + " ").split("\\p{P}?[ \\t\\n\\r]+");
        for (String word: parts) {
            synchronized (lock) {
                if (!hashDictionary.add(word) && isWork()) {
                    LOGGER.warn("Duplicate word found: " + word);
                    Main.setShutdown();
                }
            }
        }
    }


}
