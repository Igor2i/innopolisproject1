package com.igor2i.innopolis.project1.threads;

import com.igor2i.innopolis.project1.parsers.resource.Resource;
import com.igor2i.innopolis.project1.parsers.wordInCollection.ParseWord;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Данный класс имплементирует Runnable
 * И работае в режиме Producer
 * @author  igor2i on 08.02.17.
 */
public class ResourceProducer implements Runnable {

    private static final Logger LOGGER = LogManager.getLogger(ResourceProducer.class);

    @Deprecated
    private Collection<String> queue;

    private Object lock;
    private HashSet<String> hashDictionary;
    private Resource resource;
    private AtomicInteger countResources;


    /**
     * При обявление класса указывается файл ресурса откуда будет производиться чтение данных
     * и Коллекция куда буду помещаться данные
     * @param lock
     * @param hashDictionary
     * @param resource
     * @param countResources
     */
    public ResourceProducer(Object lock, HashSet<String> hashDictionary, Resource resource, AtomicInteger countResources) {
        this.lock = lock;
        this.hashDictionary = hashDictionary;
        this.resource = resource;
        this.countResources = countResources;
    }

    @Override
    public void run() {
        LOGGER.info("Start thread with word " + resource.getResourceName());
        InputStreamReader inputSR = resource.getInputStreamReader();
        ParseWord parseWord = new ParseWord(lock, inputSR, hashDictionary);
        parseWord.newParse();
        countResources.decrementAndGet();

    }

}
