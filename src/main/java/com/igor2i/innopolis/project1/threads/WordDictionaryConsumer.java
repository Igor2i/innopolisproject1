package com.igor2i.innopolis.project1.threads;

import com.igor2i.innopolis.project1.Main;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashSet;
import java.util.concurrent.ConcurrentLinkedQueue;

import static com.igor2i.innopolis.project1.Main.isWork;

/**
 * Словарь
 * @author igor2i on 08.02.17.
 */
@Deprecated
public class WordDictionaryConsumer implements Runnable {

    private static final Logger LOGGER = LogManager.getLogger(WordDictionaryConsumer.class);

    private HashSet<String> wordDictionary = new HashSet<>();
    private final ConcurrentLinkedQueue<String> queue;

    /**
     * Данный класс создаёт список уникальных слов, принимает данные через ConcurrentLinkedQueue<String>
     * При нахождении дубликата пвыдаёт предупреждающее сообщение, и завершает работу программы
     * @param queue
     */
    public WordDictionaryConsumer(ConcurrentLinkedQueue<String> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        LOGGER.info("Start word catcher");

        while (isWork()) {
            String word = queue.poll();

            if (word != null && isWork()) {

                LOGGER.debug("added: " + word);

                if (!wordDictionary.add(word)) {
                    LOGGER.warn("Duplicate word found: " + word);
                    Main.setShutdown();
                }
            }
        }
    }


}
