package com.igor2i.innopolis.project1;

import com.igor2i.innopolis.project1.parsers.ParseFromUriIntoArrayResource;
import com.igor2i.innopolis.project1.parsers.resource.Resource;
import com.igor2i.innopolis.project1.threads.ResourceProducer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Основной класс запускающий все потоки
 *
 * @author igor2i
 */
public class Main {

    private static final Logger LOGGER = LogManager.getLogger(Main.class);

    // Для примера добавил 2 файла с путями ROOT_FILE_NAME, один содержит лишь одно повторяющееся слово, в другомм несколько стишков.
    // можно сделать не константой, а принятием пееменной при запуске
//    private static final String ROOT_FILE_NAME = "./files/files.txt";
    private static final String ROOT_FILE_NAME = "./files/files_dictionary";
//    private static final String ROOT_FILE_NAME = "./files/files_dictionary_notReplace";
//    private static final String ROOT_FILE_NAME = "./files/urlLinks";
//    private static final String ROOT_FILE_NAME = "./files/libOfBabelLinks";

    private volatile static boolean work = true;

    @Deprecated
    private static ConcurrentLinkedQueue<String> wordQueue = new ConcurrentLinkedQueue<String>();

    private static HashSet<String> hashDictionary = new HashSet<>(64);

    private static AtomicInteger countResources = new AtomicInteger(0);

    private static Object lock = new Object();

    /**
     * Работа данного метода состоит из трёх этапов
     * 1) Запуск потока словоря-аккумулятора
     * 2) Проверка входящего файла и передача указанных файлов парсеру
     * 3) Ожидание, для корректного завершения всех потоков.
     * Логирование на уровне INFO выводится на консоль, в файл пишется в режиме DEBUG.
     *
     * @param args
     */
    public static void main(String args[]) {

        LOGGER.info("init Project1");

//        new Thread(new WordDictionaryConsumer(wordQueue), "Thread dictionary").start();

        ArrayList<Resource> resources = new ParseFromUriIntoArrayResource(new File(ROOT_FILE_NAME)).startParsURL();

        countResources.addAndGet(resources.size());

        resources.forEach(resource -> {
            if(isWork()){
                new Thread(new ResourceProducer(lock, hashDictionary, resource, countResources),
                "Thread reads the " + resource.getResourceName()).start();
            }
        });


        while (isWork()) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                LOGGER.error(e.getMessage());
                setShutdown();
            }
            if (countResources.get() <= 0 && isWork()) {
                LOGGER.info("Duplicate not found. It's good!");
                setShutdown();
            }
        }

    }

    public static boolean isWork() {
        return work;
    }

    public static void setShutdown() {
        LOGGER.debug("Opened threads - " + Thread.activeCount());
        LOGGER.info("Good bye!");
        Main.work = false;
    }

}

