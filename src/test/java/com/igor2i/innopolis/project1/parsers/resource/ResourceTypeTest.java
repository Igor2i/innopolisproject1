package com.igor2i.innopolis.project1.parsers.resource;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by igor2i on 14.02.17.
 */
class ResourceTypeTest {
    @Test
    void isWebType() {

        String[] arrTrueUrl = {
                "https://ru.wikipedia.org/wiki/Java",
                "http://ru.wikipedia.org/wiki/Java",
                "https://ru.wikipedia.org/wiki/%D0%A2%D0%B5%D1%81%D0%BB%D0%B0,_%D0%9D%D0%B8%D0%BA%D0%BE%D0%BB%D0%B0",
                "https://google.com",
                "ftp://test.ru"
        };
        String[] arrFalseUrl = {
                "test",
                "//google.com/",
                "\\\\frewv.erwr",
                "file.file.fi",
                "test://tewddsf.ru"
        };

        for (String url: arrTrueUrl) {
            assertTrue(ResourceType.isWebType(url) );
        }

        for (String url: arrFalseUrl) {
            assertFalse(ResourceType.isWebType(url) );
        }

    }

}