package com.igor2i.innopolis.project1.parsers.wordInCollection;


import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Created by igor2i on 14.02.17.
 */
class ParseWordTest {

    private Object lock = new Object();

    @Test
    void newParse() {

        String testStr = "Разлука тяжела нам как недуг Но временами одинокий путь Счастливейшим мечтам дает досуг";

        InputStreamReader inputStreamReader = new InputStreamReader(new ByteArrayInputStream(testStr.getBytes()));

        HashSet<String> hashSetCheck = new HashSet<>();
        ParseWord parseWord = new ParseWord(lock,inputStreamReader,hashSetCheck);
        parseWord.newParse();

        HashSet<String> controlHashSet = new HashSet<>();
        controlHashSet.addAll(Arrays.asList(testStr.split(" ")));

        System.out.println("Out parseWord");
        hashSetCheck.forEach(f -> System.out.print(f + " "));
        System.out.println();
        System.out.println("Out control HashSet");
        controlHashSet.forEach(f -> System.out.print(f + " "));

        assertEquals(controlHashSet, hashSetCheck);
    }

}